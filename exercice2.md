# Vérifier la disponbilité d’un service avec telnet

Installez la commande `telnet`. Dans l’écran Paramètres, recherchez `telnet`. Windows 10 vous propose une fenêtre `Activer ou désactiver des fonctionnalités de Windows`.

Dans la liste, descendez jusqu’à `Telnet Client`, cochez la case correspondante et cliquez sur OK.

# Se servir de `telnet`

`telnet` permet de créer une connexion TCP vers un service distant. Cela peut être utilisé pour vérifier que certains services répondent correctement et ne sont pas filtrés par un *firewall*.

Ouvrez un terminal de commande et tapez :

```
telnet www.lemonde.fr 80
```

Le terminal va devenir tout noir. Tapez quelques caractères et appuyez 2 fois sur Entrée. Vous devriez voir un message du type `Bad Request`.

*Quel port avez-vous testé ? Que pouvez-vous en déduire ?*

Essayez le port 8192 vers la même adresse.

*Que se passe-t-il ? Que pouvez-vous en déduire ?*

Réessayez avec le port 22.

*Voyez-vous une différence ? Pourquoi à votre avis ?*

Enfin, nous allons reprendre la même commande mais essayez le port 443 qui correspond au HTTPS.

*Que se passe-t-il à votre avis ? Avez-vous pu vous connecter ?*

# Retrouver mon adresse IP publique

Il n’y a pas de moyen direct de retrouver son adresse IP publique.

Rendez-vous sur [cette page Web](http://monip.org/) pour retrouver votre adresse IP publique Web.

*Attention, votre adresse de sortie Web n’est pas forcément votre adresse de sortie pour d’autres protocoles ! Vous pouvez très bien être derrière un proxy qui aura une adresse différente par exemple !*

# (collectif) Quelques éléments de diagnostic réseau

Ouvrez ce lien dans votre navigateur http://ra.mateu.be/

*Aidez-vous de tout ce que vous avez fait jusqu’à présent pour essayer de comprendre ce qui ne marche pas*

Ouvrez ce lien dans votre navigateur http://rb.mateu.be/

*Même principe…*

Ouvrez ce lien dans votre navigateur http://rc.mateu.be/

*Même principe…*

Ouvrez ce lien dans votre navigateur http://r.mateu.be/iamhere

*Même principe…*

