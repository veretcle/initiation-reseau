# Découvrir mon parcours sur Internet

Dans un terminal de commande, utilisez la commande `tracert` vers les IP suivantes :
* 163.172.112.17
* 194.156.203.252
* 87.236.185.153

À l’aide de ce (site)[https://bgp.he.net], essayez de détermine votre parcours précis sur Internet : numéro d’AS que vous traversez, nom du fournisseur, nationalité, etc… Utilisez le champs de recherche en haut pour chercher les adresses IP et cliquez sur le résultat pour voir l’opérateur à qui l’adresse ou la plage d’adresses appartient.

# Effectuer des résolutions de nom

## Résolution basique de nom

Sous Windows, la commande `nslookup.exe` permet de faire des résolutions de nom. Dans un terminal, tapez la commande `nslookup.exe`. Vous allez alors passer dans un terminal spécial qui ne fait que des résolutions de nom.

Essayez de résoudre les noms suivants (il suffit de les taper dans le terminal `nslookup.exe`) :
* www.qwant.com
* bgp.he.net
* test.giteu.be
* gmail.com
* www.lemonde.fr

Par défaut, `nslookup.exe` ne résout que les enregistrements `A` (adresse IPv4). Nous allons le forcer à renvoyer les enregistrements IPv6 (`AAAA`) :

```
> set type=aaaa
```

Refaites les même résolutions que précédemment.

Pour finir, reforcez les résolutions IPv4 (tapez simplement `set type=a`)et résolvez les noms que nous avions essayé à l’exercice 2.

## Trouver les serveurs faisant autorité


Pour trouver les serveurs faisant autorité, il faut modifier le type d’enregistrements pour `NS` (*Name Server*). Essayez de trouver quel est le prestataire de service qui héberge les zones du chapitre précédent.

## Trouver le(s) serveur(s) de messagerie d’un domaine

L’enregistrement `MX` permet de connaître les serveurs de messagerie d’un domaine particulier. Forcez `nslookup.exe` à résoudre les enregistrements `MX` (`set type=mx`) et essayez de découvrir quels serveurs sont responsables de l’acheminement de la messegarie pour les domaines du chapitre précédent.

Une fois que vous avez découvert les noms des serveurs, trouvez leurs adresses IPv4/IPv6 et allez chercher l’AS qui les héberge.

# (collectif) Illustration du principe des TTL

L’un d’entre vous va résoudre le nom affiché au tableau.

*Les autres peuvent-ils résoudre le nom en question ? Que se passe-t-il exactement ?*

Ce nom va être changé tout de suite après.

*Que donne votre résolution ? Quand va-t-elle changer ?*

