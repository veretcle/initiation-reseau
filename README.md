La présentation du cours est accessible [ici](https://veretcle.frama.io/initiation-reseau/).

Les exercices sont accessibles ci-dessous :
* [Exercice 1](https://framagit.org/veretcle/initiation-reseau/blob/master/exercice1.md)
* [Exercice 2](https://framagit.org/veretcle/initiation-reseau/blob/master/exercice2.md)
* [Exercice 3](https://framagit.org/veretcle/initiation-reseau/blob/master/exercice3.md)
* [Exercice 4](https://framagit.org/veretcle/initiation-reseau/blob/master/exercice4.md)