# Différents temps de réponse

Utilisez la commande `ping` avec l’option `-n 30` pour mesurer de manière précise le temps de réponse vers ces différentes adresses IP :
* 217.109.234.94 (France, Angoulême)
* 185.118.18.193 (France, Toulon)
* 81.80.59.86 (France, Lille)
* 198.27.92.1 (France, Lille)
* 87.236.185.153 (Russie, St Petersbourg)
* 181.49.100.9 (Colombie, Bogotà)
* 190.216.184.1 (Pérou, Lima)
* 177.21.5.137 (Brésil, Sao Paulo)
* 175.28.3.169 (Cambodge, Phnom Penh)
* 163.53.185.18 (Japon, Osaka)

*Que remarquez-vous globalement ? Quel autre facteur à part la distance peut influer sur le temps de réponse ?*

# Mesures de débit

Allez récupérer [iPerf](https://iperf.fr/download/windows/iperf-3.1.3-win64.zip), dézippez l’exécutable. `iPerf` permet de mesurer la bande passante montant entre client et serveur (cela signifie que c’est le client qui envoie les données).

Demandez son adresse IP à l’un de vos voisins et demandez-lui de démarrer `iPerf` en mode serveur. Pour ce faire, dans un terminal de commande, allez dans le dossier où vous avez dézippé `iPerf` et tapez la commande suivante :

```
> cd Downloads\iperf-3.1.3-win64\iperf-3.1.3-win64\
> iperf3.exe -s
```
*Note : le pare-feux de Windows vous demandera peut-être s’il faut autoriser `iperf3.exe` à ouvrir un port. Cochez l’ensemble des cases et acceptez.*

Cette commande fait de vous un serveur `iPerf` et affichera les résultats des tests de bande passante toutes les secondes.

Côté client, allez dans le même dossier et tapez la commande suivante :

```
> iperf3.exe -c <adresse IP du serveur> -t 30
```

`iPerf` va alors essayer de transférer le plus de données possible du client vers le serveur (bande passante montante donc du client vers le serveur) pendant 30 secondes en vous affichant les résultats intermédiaires toutes les secondes.

*Remarquez que les résultats vous sont indiqués de deux manières différentes : la quantité de données transférées en *MBytes* ou mega-octets et le taux de transfert en *Mbits/s* soit megabits par seconde !*

Renouvellez l’expérience mais avec l’un de ces serveurs `iPerf` publics :
* `ping.online.net`
* `bouygues.iperf.fr`
* `speedtest.serverius.net`
* `iperf.eenet.ee`
* `iperf.volia.net`

*Note : il faut parfois ajouter l’option `-p` et un port compris entre 5200 et 5209 pour que cela fonctionne.*

*Que se passe-t-il si vous essayez tous en même temps de faire un `iPerf` vers un serveur public ?*
