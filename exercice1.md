# Je trouve mon adresse MAC, mon adresse IP, mon masque de sous-réseau et ma passerelle

## Méthode graphique

Ouvrez l’écran `Paramètres` puis cliquez sur `Réseau et Internet`.

Dans le panneau de droite, cliquez sur `Ethernet` ou `Wi-Fi`, puis dans le panneau de gauche, cliquez sur `Modifier les options d’adaptateur`.

Dans la nouvelle fenêtre, vous devriez voir plusieurs cartes réseaux. Repérez les cartes réseaux connectées, puis faites un clic-droit dessus, et sélectionnez `Statut`.

Dans cette fenêtre, cliquez sur le bouton `Détails…`.

Votre adresse MAC est indiquée sou le nom `Adresse physique`.

Trouvez votre adresse IPv4, votre adresse IPv6, votre masque de sous-réseau et votre adresse de passerelle.

## Méthode non-graphique

Ouvrir un terminal de commande : Menu Windows, recherchez `cmd`.

Tapez la commande :
```
ipconfig /all
```

Cette commande donne la configuration de toutes les cartes réseaux, dans le détail. En tapant la commande sans le `/all`, nous n’obtenez pas toutes les informations.

Trouvez la ligne marquée `Adresse physique`. Cela correspond à l’adresse MAC (la notation est donnée avec des `-` et non des `:` comme c’est l’usage…).

Trouvez votre adresse IPv4, votre adresse IPv6, votre masque de sous-réseau et votre adresse de passerelle.

*Vous trouverez aussi une *adresse de lien local*. Quel est sa particularité ?*

# Je calcule mon adresse de réseau

À partir des informations récupérées précédemment, calculez votre adresse de réseau.

*Est-ce que la passerelle que vous avez vu est bien contenu à l’intérieur de votre sous-réseau ?*

Déduisez-en l’adresse la plus grande et l’adresse la plus petite du réseau.

*Vous pouvez vous appuyer sur une calculette IP en ligne si nécessaire.*

# J’observe le réseau local autour de moi

Dans un terminal de commande, tapez la commande suivante :

```
arp -a
```

Cela va vous permettre de voir toutes les associations MAC/IPv4 sur le réseau local.

*Voyez-vous votre adresse de passerelle ?*

**Demandez son adresse à un ou plusieurs de vos voisins.**

*La commande `ping` utilise le protocole ICMP et sert, entre autres, à vérifier le temps de réponse d’une machine ou vérifier qu’elle est accessible.*

Dans le même terminal de commande, tapez la commande :

```
ping <IPv4 d’un de mes voisins>
```

*Voyez-vous une réponse ? Si oui, quel temps de réponse pouvez-vous observer ?*

Retapez la commande `arp` et vérifier que vous voyez bien l’IP de votre voisin dans cette table. Même si l’adresse de votre voisin n’a pas répondu au `ping` vous devriez être en mesure de le voir apparaître dans la table ARP.

# Je vérifie le fonctionnement du DHCP

Sauf si vous l’avez entrée manuellement, votre adresse a dû être attribuée par le DHCP. Vous pouvez « libérer » cette adresse à tout moment via la commande :

```
ipconfig /release
```

*Tapez cette commande. Vérifiez votre adresse IP.*

Vous pouvez redemander manuellement une adresse au DHCP avec la commande suivante :

```
ipconfig /renew
```

*Qu’observez-vous ?*

# Je découvre une portion de réseau au-delà du réseau local

Dans le même terminal de commande, tapez la commande :

```
ping 194.156.203.252
```

*Avez-vous une réponse ? Est-ce que cette adresse apparaît dans votre table ARP locale ? Qu’en déduisez-vous ?*

