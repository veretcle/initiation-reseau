window.onload = function () {
	var s1 = Snap("#NAT_2");
	Snap.load("img/nat_2_sch.svg", function(f){
		var src_orig = f.select('#src_orig');
		var dst_orig = f.select('#dst_orig');
		var payload_orig = f.select('#payload_orig');
		var src_snat = f.select('#src_snat');
		var src_res = f.select('#src_res');
		var dst_res = f.select('#dst_res');
		var payload_res = f.select('#payload_res');
		var dst_dnat = f.select('#dst_dnat');
		var box = f.select('#box');
		var workstation = f.select('#workstation');
		var server = f.select('#server');

		// hide original packet
		src_orig.attr({ visibility: "hidden"});
		dst_orig.attr({ visibility: "hidden"});
		payload_orig.attr({ visibility: "hidden"});

		//hide NAT
		src_snat.attr({ visibility: "hidden"});

		// hide response packet
		src_res.attr({ visibility: "hidden"});
		dst_res.attr({ visibility: "hidden"});
		payload_res.attr({ visibility: "hidden"});

		// hide DNAT
		dst_dnat.attr({ visibility: "hidden"});

		workstation.click(function() {
			src_orig.attr({ visibility: ""});
			dst_orig.attr({ visibility: ""});
			payload_orig.attr({ visibility: ""});
		});

		dst_orig.click(function() {
			src_orig.animate({transform:'t300,0'}, 500, mina.linear);
			dst_orig.animate({transform:'t300,0'}, 500, mina.linear);
			payload_orig.animate({transform:'t300,0'}, 500, mina.linear);
		});

		payload_orig.click(function() {
			src_orig.attr({ visibility: "hidden"});
			src_snat.attr({ visibility: ""});
			src_snat.animate({transform: 't153,-80'}, 0, mina.linear);
		});

		box.click(function() {
			src_snat.animate({transform:'t353,-80'}, 500, mina.linear);
			dst_orig.animate({transform:'t500,0'}, 500, mina.linear);
			payload_orig.animate({transform:'t500,0'}, 500, mina.linear);
		});

		server.click(function() {
			src_res.attr({ visibility: ""});
			dst_res.attr({ visibility: ""});
			payload_res.attr({ visibility: ""});
		});

		payload_res.click(function() {
			src_res.animate({transform:'t-200,0'}, 500, mina.linear);
			dst_res.animate({transform:'t-200,0'}, 500, mina.linear);
			payload_res.animate({transform:'t-100,-80'}, 500, mina.linear);
		});

		dst_res.click(function() {
			dst_res.attr({ visibility: "hidden"});
			dst_dnat.attr({ visibility: ""});
			dst_dnat.animate({transform:'t43,-75'}, 0, mina.linear);
		});
		s1.append(f);
	});

	var s2 = Snap('#delegation');
	Snap.load('img/delegation.svg', function(f){
		s2.append(f);
	})

	var s3 = Snap('#routage');
	Snap.load('img/routage.svg', function(f) {
		var router0 = f.select('#router0');
		var router1 = f.select('#router1');
		var router3 = f.select('#router3');
		var arrow1 = f.select('#arrow1');
		var arrow2 = f.select('#arrow2');
		var arrow3 = f.select('#arrow3');

		arrow1.attr({ visibility: "hidden" });
		arrow2.attr({ visibility: "hidden" });
		arrow3.attr({ visibility: "hidden" });

		router0.click(function() {
			arrow1.attr({ visibility: "" });
		});

		router1.click(function() {
			arrow2.attr({ visibility: "" });
		});

		router3.click(function() {
			arrow3.attr({ visibility: "" });
		});

		s3.append(f);
	});
};
